"""REST client handling, including PayPalStream base class."""

import requests
from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from memoization import cached

from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream

from tap_paypal.auth import PayPalAuthenticator
from datetime import datetime, timedelta




class PayPalStream(RESTStream):
    """PayPal stream class."""
    page = 1
    @property
    @cached
    def url_base(self) -> str:
        """Return the API URL root, configurable via tap settings."""
        if self.config.get('sandbox',False):
            return "https://api-m.sandbox.paypal.com"
        else:
            return "https://api-m.paypal.com"
        

    records_jsonpath = "$[*]"  
    next_page_token_jsonpath = "$.next_page"  

    @property
    @cached
    def authenticator(self) -> PayPalAuthenticator:
        """Return a new authenticator object."""
        return PayPalAuthenticator.create_for_stream(self,f"{self.url_base}/v1/oauth2/token")

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        next_page_token = None

        data = response.json()
        if "total_pages" in data:
            if self.page > data['total_pages']:
                return None
            self.page = self.page+1
            next_page_token = self.page    

        return next_page_token

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        if next_page_token:
            params["page"] = next_page_token
        if self.name=="transactions":
            start_date = self.get_starting_timestamp(context) or datetime(2023, 1, 1)
            if self.config.get("start_date") and not start_date:
                start_date = datetime.strptime(
                    self.config.get("start_date"), "%Y-%m-%dT%H:%M:%S.%fZ"
                )
            start_date = start_date
            current_date = datetime.now()
            difference = current_date - start_date
            difference = difference.days
            if difference>31:
                #Maximum lookback allowed by Paypal
                current_date = start_date + timedelta(days=31)
            start_date = start_date.strftime("%Y-%m-%dT%H:%M:%S")
            current_date = current_date.strftime("%Y-%m-%dT%H:%M:%S")
            params['start_date'] = f"{start_date}.999Z"
            params['end_date'] = f"{current_date}.999Z"
        return params
