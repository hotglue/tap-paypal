"""PayPal Authentication."""


from singer_sdk.authenticators import OAuthAuthenticator, SingletonMeta
from singer_sdk.helpers._util import utc_now
import requests

# The SingletonMeta metaclass makes your streams reuse the same authenticator instance.
# If this behaviour interferes with your use-case, you can remove the metaclass.
class PayPalAuthenticator(OAuthAuthenticator, metaclass=SingletonMeta):
    """Authenticator class for PayPal."""

    @property
    def oauth_request_body(self) -> dict:
        """Define the OAuth request body for the PayPal API."""
        return {
            'scope': self.oauth_scopes,
            'grant_type': 'client_credentials',
        }

    @classmethod
    def create_for_stream(cls, stream,auth_url) -> "PayPalAuthenticator":
        return cls(
            stream=stream,
            auth_endpoint=auth_url,
            # oauth_scopes="TODO: OAuth Scopes",
        )
    
    def update_access_token(self) -> None:
        """Update `access_token` along with: `last_refreshed` and `expires_in`.

        Raises:
            RuntimeError: When OAuth login fails.
        """
        request_time = utc_now()
        auth_request_payload = self.oauth_request_payload
        token_response = requests.post(self.auth_endpoint, data=auth_request_payload, auth=(str(self.config.get('client_id')),str(self.config.get('client_secret'))))
        try:
            token_response.raise_for_status()
            self.logger.info("OAuth authorization attempt was successful.")
        except Exception as ex:
            raise RuntimeError(
                f"Failed OAuth login, response was '{token_response.json()}'. {ex}"
            )
        token_json = token_response.json()
        self.access_token = token_json["access_token"]
        self.expires_in = token_json.get("expires_in", self._default_expiration)
        if self.expires_in is None:
            self.logger.debug(
                "No expires_in receied in OAuth response and no "
                "default_expiration set. Token will be treated as if it never "
                "expires."
            )
        self.last_refreshed = request_time
