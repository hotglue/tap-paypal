"""Stream type classes for tap-paypal."""

from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th  

from tap_paypal.client import PayPalStream



class SubscriptionPlansStream(PayPalStream):
    name = "subscription_plans"
    path = "/v1/billing/plans"
    primary_keys = ["id"]
    records_jsonpath = "$.plans[*]"
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("product_id", th.StringType),
        th.Property("status", th.StringType),
        th.Property("name", th.StringType),
        th.Property("description", th.StringType),
        th.Property("create_time", th.DateTimeType),
        th.Property("links", th.CustomType({"type": ["array", "string"]})),
    ).to_dict()

class SubscriptionTransactionsStream(PayPalStream):
    name = "subscription_transactions"
    path = "/v1/billing/plans"
    primary_keys = ["id"]
    records_jsonpath = "$.transactions[*]"
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("status", th.StringType),
        th.Property("payer_email", th.StringType),
        th.Property("payer_name", th.ObjectType(
            th.Property("given_name", th.StringType),
            th.Property("surname", th.StringType),
        )),
        th.Property("amount_with_breakdown", th.CustomType({"type": ["object", "string"]})),
        th.Property("time", th.DateTimeType),
    ).to_dict()

class InvoicesStream(PayPalStream):
    name = "invoices"
    path = "/v2/invoicing/invoices"
    primary_keys = ["id"]
    records_jsonpath = "$.items[*]"
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("status", th.StringType),
        th.Property("detail", th.ObjectType(
            
            th.Property("invoice_number", th.StringType),
            th.Property("reference", th.StringType),
            th.Property("invoice_date", th.StringType),
            th.Property("currency_code", th.StringType),
            th.Property("note", th.StringType),
            th.Property("term", th.StringType),
            th.Property("memo", th.StringType),
            th.Property("payment_term", th.CustomType({"type": ["object", "string"]})),
            th.Property("payment_term", th.CustomType({"type": ["object", "string"]})), 
        )),
        th.Property("invoicer", th.CustomType({"type": ["object", "string"]})),
        th.Property("primary_recipients",th.CustomType({"type": ["array", "object", "string"]})),
        th.Property("amount", th.CustomType({"type": ["object", "string"]})),
        th.Property("links", th.CustomType({"type": ["array", "string"]})),
        th.Property("time", th.DateTimeType),
    ).to_dict()

class ProductsStream(PayPalStream):
    name = "products"
    path = "/v1/catalogs/products"
    primary_keys = ["id"]
    records_jsonpath = "$.products[*]"
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("name", th.StringType),
        th.Property("description", th.StringType),
        th.Property("create_time", th.DateTimeType),
        th.Property("links", th.CustomType({"type": ["array", "string"]})),
        
    ).to_dict()
class TransactionsStream(PayPalStream):
    name = "transactions"
    path = "/v1/reporting/transactions"
    primary_keys = ["id"]
    records_jsonpath = "$.transaction_details[*]"
    replication_key = None
    schema = th.PropertiesList(
        th.Property("transaction_info", th.ObjectType(
            th.Property("paypal_account_id", th.StringType),
            th.Property("transaction_id", th.StringType),
            th.Property("transaction_event_code", th.StringType),
            th.Property("transaction_initiation_date", th.DateTimeType),
            th.Property("transaction_updated_date", th.DateTimeType),
            th.Property("transaction_amount", th.CustomType({"type": ["object", "string"]})),
            th.Property("fee_amount", th.CustomType({"type": ["object", "string"]})),
            th.Property("transaction_status", th.StringType),
            th.Property("protection_eligibility", th.StringType),
            th.Property("instrument_type", th.StringType),
            th.Property("instrument_sub_type", th.StringType),
        )),
        th.Property("payer_info", th.CustomType({"type": ["object", "string"]})),
        th.Property("shipping_info", th.CustomType({"type": ["object", "string"]})),
        th.Property("cart_info", th.CustomType({"type": ["object", "string"]})),
        th.Property("store_info", th.CustomType({"type": ["object", "string"]})),
        th.Property("auction_info", th.CustomType({"type": ["object", "string"]})),
        th.Property("incentive_info", th.CustomType({"type": ["object", "string"]})),
        
    ).to_dict()
    def post_process(self, row: dict, context: Optional[dict] = None) -> Optional[dict]:
        #@TODO populate replication key is transaction_updated_date always have a value in live data
        return row
