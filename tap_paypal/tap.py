"""PayPal tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th  # JSON schema typing helpers
# TODO: Import your custom stream types here:
from tap_paypal.streams import (
    PayPalStream,
    SubscriptionPlansStream,
    SubscriptionTransactionsStream,
    InvoicesStream,
    ProductsStream,
    TransactionsStream,
)

STREAM_TYPES = [
    SubscriptionPlansStream,
    SubscriptionTransactionsStream,
    InvoicesStream,
    ProductsStream,
    TransactionsStream,
]


class TapPayPal(Tap):
    """PayPal tap class."""
    name = "tap-paypal"

    # TODO: Update this section with the actual config values you expect:
    config_jsonschema = th.PropertiesList(
        th.Property(
            "client_id",
            th.StringType,
            required=True
        ),
        th.Property(
            "client_secret",
            th.StringType,
            required=True
        ),
        #enable later
        # th.Property(
        #     "refresh_token",
        #     th.StringType,
        #     required=True
        # ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]

if __name__ == '__main__':
    TapPayPal.cli()    
